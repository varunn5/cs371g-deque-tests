// -------------
// TestDeque.c++
// -------------

// http://www.cplusplus.com/reference/deque/deque/

// --------
// includes
// --------

#include <deque>     // deque
#include <stdexcept> // invalid_argument, out_of_range

#include "gtest/gtest.h"

#include "Deque.hpp"

// -----
// Using
// -----

using namespace std;
using namespace testing;

// -----
// Types
// -----

template <typename T>
struct DequeFixture : Test {
    using deque_type    = T;
    using allocator_type = typename deque_type::allocator_type;
    using iterator       = typename deque_type::iterator;
    using const_iterator = typename deque_type::const_iterator;
};

using
deque_types =
    Types<
    deque<int>,
    my_deque<int>,
    deque<int, allocator<int>>,
    my_deque<int, allocator<int>>>;

#ifdef __APPLE__
TYPED_TEST_CASE(DequeFixture, deque_types,);
#else
TYPED_TEST_CASE(DequeFixture, deque_types);
#endif

// -----
// Tests
// -----

/**
 * Tests if size is empty
 **/
TYPED_TEST(DequeFixture, test1) {
    using deque_type = typename TestFixture::deque_type;
    const deque_type x;
    ASSERT_TRUE(x.empty());
    ASSERT_EQ(x.size(), 0u);
}
/**
 * Tests iterator
 **/
TYPED_TEST(DequeFixture, test2) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x;
    iterator b = begin(x);
    iterator e = end(x);
    EXPECT_TRUE(b == e);
}
/**
 * Tests const_iterator
 **/
TYPED_TEST(DequeFixture, test3) {
    using deque_type     = typename TestFixture::deque_type;
    using const_iterator = typename TestFixture::const_iterator;
    const deque_type x;
    const_iterator b = begin(x);
    const_iterator e = end(x);
    EXPECT_TRUE(b == e);
}
/**
 * Tests empty deque equality
 **/
TYPED_TEST(DequeFixture, test4) {
    using deque_type = typename TestFixture::deque_type;
    // using iterator   = typename TestFixture::iterator;
    deque_type x;
    deque_type y;
    EXPECT_TRUE((x == y));
}
/**
 * Tests iterator on filled deque
 **/
TYPED_TEST(DequeFixture, test5) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x(8, 5);
    iterator beg = x.begin();
    iterator end = (beg+1);
    iterator cen = (++beg);
    EXPECT_TRUE(cen == end);
}
/**
 * Tests iterator on filled deque
 **/
TYPED_TEST(DequeFixture, test6) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x(8, 5);
    iterator beg = x.begin();
    iterator end = (beg+2);
    iterator cen = beg + 2;
    EXPECT_TRUE(cen == end);
}
/**
 * Tests iterator on filled deque
 **/
TYPED_TEST(DequeFixture, test7) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x(9, 10);
    iterator end = x.end();
    iterator cur = (end-1);
    iterator sol = --end;
    EXPECT_TRUE(cur == sol);
}
/**
 * Tests iterator on filled deque
 **/
TYPED_TEST(DequeFixture, test8) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x(8, 6);
    iterator end = x.end();
    iterator cur = (end-2);
    iterator sol = --(--end);
    EXPECT_TRUE(cur == sol);
}
/**
 * Tests pushback and at
 **/
TYPED_TEST(DequeFixture, test9) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x(4, 2);
    iterator b = x.begin();
    x.push_back(7);
    int j = x.at(4);
    EXPECT_TRUE(j == 7);
}

/**
 * Tests pushback and at
 **/
TYPED_TEST(DequeFixture, test10) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x(4, 2);
    iterator b = x.begin();
    x.push_back(8);
    int j = x.at(4);
    EXPECT_TRUE(j == 8);
}
/**
 * Tests * operator iterator
 **/
TYPED_TEST(DequeFixture, test11) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x(3, 1);
    iterator beg = x.begin();
    EXPECT_TRUE((*beg == 1));
}
/**
 * Tests * ++ operator iterator
 **/
TYPED_TEST(DequeFixture, test12) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x(3, 1);
    iterator beg = x.begin();
    EXPECT_TRUE((*(++beg) == 1));
}
/**
 * Tests *  - ==operator iterator
 **/
TYPED_TEST(DequeFixture, test13) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x(3, 1);
    iterator beg = x.end();
    EXPECT_TRUE((*(beg-1) == 1));
}
/**
 * Tests *  operator iterator
 **/
TYPED_TEST(DequeFixture, test14) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x(4, 1);
    iterator beg = x.begin();
    EXPECT_TRUE((*beg == 1));
}
/**
 * Tests *  operator iterator
 **/
TYPED_TEST(DequeFixture, test15) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x(5, 1);
    iterator beg = x.begin();
    *beg = 5;
    EXPECT_TRUE((*beg == 5));
}
/**
 * Tests pushback
 **/
TYPED_TEST(DequeFixture, test16) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x;
    x.push_back(0);
    ASSERT_EQ(x.size(), 1);
}
/**
 * Tests size and back
 **/
TYPED_TEST(DequeFixture, test17) {
    using deque_type = typename TestFixture::deque_type;
    // using iterator   = typename TestFixture::iterator;
    deque_type x(4,2);
    x.resize(3);
    int a = x.back();
    ASSERT_EQ(a,2);
    ASSERT_EQ(x.size(),3);

}

/**
 * Tests resize and back
 **/
TYPED_TEST(DequeFixture, test18) {
    using deque_type = typename TestFixture::deque_type;
    using deque_type = typename TestFixture::deque_type;
    // using iterator   = typename TestFixture::iterator;
    deque_type x(3,2);
    x.resize(8,3);
    ASSERT_EQ(x.size(),8);
    int a = x.back();
    ASSERT_EQ(a,3);

}
/**
 * Tests pushback and back
 **/
TYPED_TEST(DequeFixture, test19) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x(4,2);
    x.push_back(1);
    int a = x.back();
    ASSERT_EQ(a,1);
    ASSERT_EQ(x.size(),5);


}
/**
 * Tests popfront
 **/
TYPED_TEST(DequeFixture, test20) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x(3,2);
    x.pop_front();
    ASSERT_EQ(x.size(),2);

}
/**
 * Tests popfront
 **/
TYPED_TEST(DequeFixture, test21) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x(8,2);
    x.pop_front();
    ASSERT_EQ(x.size(),7);
}
/**
 * Tests popfront
 **/
TYPED_TEST(DequeFixture, test22) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x(9,2);
    x.pop_front();
    ASSERT_EQ(x.size(),8);
}
/**
 * Tests popfront
 **/
TYPED_TEST(DequeFixture, test23) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x(10,2);
    x.pop_front();
    ASSERT_EQ(x.size(),9);
}/**
 * Tests popback
 **/

TYPED_TEST(DequeFixture, test24) {
    using deque_type = typename TestFixture::deque_type;
    // using iterator   = typename TestFixture::iterator;
    deque_type x(5,2);
    x.pop_back();
    ASSERT_EQ(x.size(),4);

}

/**
 * Tests popback
 **/
TYPED_TEST(DequeFixture, test25) {
    using deque_type = typename TestFixture::deque_type;
    // using iterator   = typename TestFixture::iterator;
    deque_type x(6,2);
    x.pop_back();
    ASSERT_EQ(x.size(),5);

}
/**
 * Tests popback
 **/
TYPED_TEST(DequeFixture, test26) {
    using deque_type = typename TestFixture::deque_type;
    // using iterator   = typename TestFixture::iterator;
    deque_type x(9,2);
    x.pop_back();
    ASSERT_EQ(x.size(),8);

}
/**
 * Tests popback
 **/
TYPED_TEST(DequeFixture, test27) {
    using deque_type = typename TestFixture::deque_type;
    // using iterator   = typename TestFixture::iterator;
    deque_type x(10,2);
    x.pop_back();
    ASSERT_EQ(x.size(),9);

}
/**
 * Tests popback
 **/
TYPED_TEST(DequeFixture, test28) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x(10, 1);
    x.resize(19);
    ASSERT_EQ(x.size(), 19);
    ;
}
/**
 * Tests popback
 **/
TYPED_TEST(DequeFixture, test29) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x;
    x.push_back(2);
    x.pop_front();
    ASSERT_TRUE(x.empty());


}
/**
 * Tests popback
 **/
TYPED_TEST(DequeFixture, test30) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x(8, 1);
    for(int i = 0; i < 8; ++i) {
        x.pop_front();
    }
    ASSERT_EQ(x.size(), 0);

}
/*
 *  checks that size 1 front and back pointers are the same Tests popback
 */

TYPED_TEST(DequeFixture, test31) {
    using deque_type = typename TestFixture::deque_type;
    const deque_type x (1, 1);
    ASSERT_EQ(x.front(), x.back());
}


/**
 * Tests if size is one on cost
 **/
TYPED_TEST(DequeFixture, test32) {
    using deque_type = typename TestFixture::deque_type;
    const deque_type x(1);
    ASSERT_TRUE(x.size() == 1);
}

/**
 * Tests index works in const deque
 **/
TYPED_TEST(DequeFixture, test33) {
    using deque_type = typename TestFixture::deque_type;
    const deque_type y(1);
    ASSERT_EQ(0, y[0]);
}
/**
 * Tests error exception
 **/
TYPED_TEST(DequeFixture, test34) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x;
    try {
        x.at(0);
    } catch (const std::exception& e) {
    }
}
/**
 * Tests size
 **/
TYPED_TEST(DequeFixture, test35) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x(30);
    EXPECT_TRUE(x.size() == 30);
}
/**
 * Tests size
 **/
TYPED_TEST(DequeFixture, test36) {
    using deque_type = typename TestFixture::deque_type;
    const deque_type x(30);
    EXPECT_EQ(x.size(), 30);
}
/**
 * Tests size after push and pop
 **/
TYPED_TEST(DequeFixture, test37) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x(2, 2);
    for (int i = 0; i < 10; ++i) {
        x.push_back(10);
        x.pop_back();
    }
    ASSERT_EQ(x.size(), 2);
}
/**
 * Tests size
 **/
TYPED_TEST(DequeFixture, test38) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x(10, 7);
    x.insert(x.begin() + 4, 6);
    EXPECT_TRUE(x.at(4) == 6);

}
/**
 * Tests < operator
 **/
TYPED_TEST(DequeFixture, test39) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x;
    deque_type y;
    x.push_back(5);
    y.push_back(6);
    EXPECT_TRUE(x < y);
}
/**
 * Tests == operator
 **/
TYPED_TEST(DequeFixture, test40) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x;
    deque_type y;
    x.push_back(4);
    y.push_back(4);
    EXPECT_TRUE(x == y);
}







